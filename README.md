# SMB tool for the Bunny

* Author: dark_pyrro
* Props: Anyone that has worked with this before (on the original Hak5 script)
* Version: Version 1.0
* Target: Windows 10
* Category: Exfiltration
* Attackmodes: HID, Ethernet
 
## Description

**NOTE!** This was written in the end of November 2021. Things happen over time and it's not guaranteed to work if you try it 2 years later (or more). The Bunny is also currently based on a Debian version that is dated and unsupported which makes things more and more difficult to install as time passes by.

Uses the latest (as per Nov 2021) version of Impacket along with Python 3 and OpenSSL 3

See the **wiki** in this repo for more instructions on how to set things up
